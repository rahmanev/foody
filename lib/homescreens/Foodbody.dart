import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:master_project/widgets/bigtexts.dart';
import 'package:master_project/widgets/iconwithtext.dart';
import 'package:master_project/widgets/smalltext.dart';

class FoodBody extends StatefulWidget {
  const FoodBody({Key? key}) : super(key: key);

  @override
  _FoodBodyState createState() => _FoodBodyState();
}

class _FoodBodyState extends State<FoodBody> {
  PageController _pageController = PageController(viewportFraction: 0.85);
  double _currentpageevalue = 0;
  @override
  void initState() {
    super.initState();
    _pageController.addListener(() {
      setState(() {
        _currentpageevalue = _pageController.page!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          // color: Colors.red,
          height: 300,
          child: PageView.builder(
              controller: _pageController,
              itemCount: 5,
              itemBuilder: (context, index) {
                var scale = (1 - (_currentpageevalue - index).abs());
                return TweenAnimationBuilder(
                    duration: Duration(milliseconds: 300),
                    tween: Tween(begin: scale, end: scale),
                    builder: (context, value, child) => Transform.scale(
                          scale: scale,
                          child: child,
                        ),
                    child: _pagebuild());
              }),
        ),
        new DotsIndicator(
          dotsCount: 5,
          position: _currentpageevalue,
          decorator: DotsDecorator(
            activeColor: Colors.blue.shade300,
            size: const Size.square(9.0),
            activeSize: const Size(18.0, 9.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
          ),
        )
      ],
    );
  }

  Widget _pagebuild() {
    return Stack(children: [
      Align(
        alignment: Alignment.topCenter,
        child: Container(
          height: 220,
          width: 340,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/schnitzel.jpg'),
                  fit: BoxFit.cover),
              color: Colors.indigoAccent,
              borderRadius: BorderRadius.circular(20)),
        ),
      ),
      Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 7,
                ),
                Btext(text: 'Chinees Foods', colorr: Colors.black),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Wrap(
                      children: List.generate(
                          5,
                          (index) => Icon(
                                Icons.star,
                                size: 18,
                                color: Colors.blue[300],
                              )),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Stext(colorr: Colors.grey, text: '4.5    1257 Comments')
                  ],
                ),
                SizedBox(
                  height: 24,
                ),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Icbutton(
                      icon: Icons.circle,
                      text: 'Normal',
                      iconcolor: Colors.yellow.shade600),
                  SizedBox(
                    width: 13,
                  ),
                  Icbutton(
                      icon: Icons.location_on,
                      text: '1.7km',
                      iconcolor: Colors.blue.shade300),
                  SizedBox(
                    width: 24,
                  ),
                  Icbutton(
                      icon: Icons.lock_clock,
                      text: '32 min',
                      iconcolor: Colors.red.shade600),
                ])
              ],
            ),
          ),
          margin: EdgeInsets.only(bottom: 8),
          width: 272,
          height: 140,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFe8e8e8),
                  offset: Offset(0, 6),
                  blurRadius: 5),
              BoxShadow(color: Colors.white)
            ],
            color: Colors.white,
          ),
        ),
      )
    ]);
  }
}
