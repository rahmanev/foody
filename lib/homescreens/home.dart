import 'package:flutter/material.dart';
import 'package:master_project/homescreens/Foodbody.dart';
import 'package:master_project/widgets/bigtexts.dart';
import 'package:master_project/widgets/smalltext.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 40, bottom: 40),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Btext(text: 'Bangladesh', colorr: Colors.blue[300]),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child:
                                Stext(colorr: Colors.black54, text: 'Mumbai'),
                          ),
                          Icon(Icons.keyboard_arrow_down)
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    width: 205,
                  ),
                  Container(
                    width: 50,
                    height: 50,
                    child: IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.search,
                          color: Colors.white,
                        )),
                    decoration: BoxDecoration(
                        color: Colors.blue[200],
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                  )
                ],
              ),
            ),
            FoodBody(),
          ],
        ),
      ),
    );
  }
}
