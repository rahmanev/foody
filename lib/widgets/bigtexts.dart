import 'dart:ui';

import 'package:flutter/material.dart';

class Btext extends StatelessWidget {
  final text;
  final colorr;
  const Btext({Key? key, required this.text, required this.colorr})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style:
          TextStyle(color: colorr, fontWeight: FontWeight.bold, fontSize: 20),
    );
  }
}
