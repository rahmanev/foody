import 'package:flutter/material.dart';

class Icbutton extends StatelessWidget {
  final IconData icon;
  final String text;
  final Color iconcolor;
  const Icbutton(
      {Key? key,
      required this.icon,
      required this.text,
      required this.iconcolor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          icon,
          color: iconcolor,
        ),
        Text(
          text,
          style: TextStyle(color: Colors.grey),
        )
      ],
    );
  }
}
