import 'package:flutter/widgets.dart';

class Stext extends StatelessWidget {
  final Color colorr;
  final String text;
  const Stext({Key? key, required this.colorr, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(
            color: colorr, fontWeight: FontWeight.bold, fontSize: 14));
  }
}
